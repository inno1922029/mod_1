from dataclasses import dataclass

@dataclass
class Weather:
    city: str
    temperature: float
    humidity: int
    pressure: int
import re, aiohttp, asyncio
from weather import Weather

def get_cities() :
    cities:list[str]
    with open("cities.txt", encoding="utf-8") as file:
        cities = re.findall(r"[a-zA-Z]+\s[a-zA-Z]+|[a-zA-Z]+", file.read())
    return cities

openweatherAPI = '0fb610dab7456bc44dbdde2ddba9be71'

async def main():
    async with aiohttp.ClientSession('https://api.openweathermap.org') as session:
        for city in get_cities():
            params = {'q': city, 'appid': openweatherAPI, 'units': 'metric'}
            async with session.get('/data/2.5/weather?', params = params) as resp:
                if (resp.status == 200):
                    json = await resp.json()
                    weather = Weather(json['name'],
                                      json['main']['temp'],
                                      json['main']['humidity'],
                                      json['main']['pressure'])
                    print(weather)
                else:
                    print(f'Информация о погоде в {city} не получена')

asyncio.run(main())